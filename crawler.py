# -*- coding: utf-8 -*-
import __future__
import requests
import re
import time
from lxml import html
from urllib import parse as urlparse

class Crawler:
    def __init__(self, url, outputfile='sitemap.xml', outputformat='xml'):
        self.url = url
        self.outputformat = outputformat
        self.outputfile = outputfile
        self.urls = set([url])
        self.visited = set([url])
        self.exts = ['htm', 'php']
        self.allowed_regex = '\.((?!htm)(?!php)\w+)$'
        self.errors = {'404': []}

    """Sets possible file extensions on the url such as htm, and php"""
    def set_exts(self, exts):
        self.exts = exts

    """Regular expression checker for urls"""
    def allow_regex(self, regex=None):
        if regex is not None:
            self.allowed_regex = regex
        else:
            allowed_regex = ''
            for ext in self.exts:
                allowed_regex += '(!{})'.format(ext)
            self.allowed_regex = '\.({}\w+)$'.format(allowed_regex)

    """Specifies the crawling process"""
    def crawlit(self, echo=False, pool_size=1):
        self.echo = echo
        self.regex = re.compile(self.allowed_regex)

        print('Parsing URL')

        self.pool = [None]
        while len(self.urls) > 0:
            self.parse()
        self.compose_xml()

    """Function to parse the URL"""
    def parse(self):
        if self.echo:
            n_visited, n_urls, n_pool = len(self.visited), len(self.urls), len(self.pool)
            status = (
                '{} Parsed pages - {} queued pages'.format(n_visited, n_urls),
                '{} Parsed pages - {} parsing processes  :: {} queued pages'.format(n_visited, n_pool, n_urls)
            )
            print(status[int()])

        if not self.urls:
            return
        else:
            url = self.urls.pop()
            try:
                response = requests.get(url)
                if response.status_code != 200:
                    if self.errors.get(str(response.status_code), False):
                        self.errors[str(response.status_code)].extend([url])
                    else:
                        self.errors.update({str(response.status_code): [url]})
                    print("Error {} at url {}".format(response.status_code, url))
                    return

                try:
                    tree = html.fromstring(response.text)
                except ValueError as e:
                    print(e)
                    tree = html.fromstring(response.content)
                for link_tag in tree.findall('.//a'):
                    link = link_tag.attrib.get('href', '')
                    newurl = urlparse.urljoin(self.url, link)
                    if self.is_proper(newurl):
                        self.visited.update([newurl])
                        self.urls.update([newurl])
            except Exception as e:
                print(e)

    """Checks if the url is valid"""
    def is_proper(self, url):
        oldurl = url
        if '#' in url:
            url = url[:url.find('#')]
        if url in self.visited or oldurl in self.visited:
            return False
        if self.url not in url:
            return False
        if re.search(self.regex, url):
            return False
        return True
    
    """Writes resulting xml to file"""
    def compose_xml(self): 
        of = open(self.outputfile, 'w')
        of.write('<?xml version="1.0" encoding="utf-8"?>\n')
        of.write('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">\n')
        url_string = '<url><loc>{}</loc></url>\n'
        while self.visited:
            of.write(url_string.format(self.visited.pop()))

        of.write('</urlset>')
        of.close()

"""We run the script"""
if __name__ == '__main__':
    url = 'http://www.mkyong.com/'
    outputformat = 'xml'
    outputfile = 'sitemap.xml'
    crawlit = Crawler(url=url, outputformat=outputformat, outputfile=outputfile)
    crawlit.crawlit(echo=True)

# Minimal crawler 

So, this is a script which crawls a webpage for specific **urls**, which can be specified at the end of the script, ie: __main__

We specify the **url**, **output format**, **output file**.

And call **crawlit** function, passing the aforementioned parameters.
And using **echo** to print the crawling process into console.

It needs some improvements, but anyway, let me know what You think of this.

Thank You very much
